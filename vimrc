"设置字符编码
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set termencoding=utf-8
set encoding=utf-8
"设置行号
set number
"设置Tab为4个空格
set tabstop=4
"set softtabstop=4
"设置缩进为4个空格
set shiftwidth=4
set expandtab
"#:retab（这个命令可以使页面中的Tab转换为空格）
"设置语法高亮度显示
syntax on
"设置缩进
 "set cindent
 "set smartindent
 set autoindent
"设置主题
"colorscheme molokai
colorscheme desert
"设置所有事件下，不发出声音"
"set belloff=all
"设置括号自动补全
inoremap ' ''<ESC>i
inoremap " ""<ESC>i
inoremap ( ()<ESC>i
inoremap [ []<ESC>i
inoremap { {<CR>}<ESC>O
"设置跳出自动补全的括号
func SkipPair()  
    if getline('.')[col('.') - 1] == ')' || getline('.')[col('.') - 1] == ']' || getline('.')[col('.') - 1] == '"' || getline('.')[col('.') - 1] == "'" || getline('.')[col('.') - 1] == '}'  
        return "\<ESC>la"  
    else  
        return "\t"  
    endif  
endfunc  
"将tab键绑定为跳出括号  
inoremap <TAB> <c-r>=SkipPair()<CR>
